package com.lp.auto.sdk.manager;

import com.lp.auto.sdk.po.Connector;
import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-07-13
 **/
public abstract class ProjectManager {

    /**
     * 创建项目
     *
     * @param connector
     * @param projInfo
     * @throws Exception
     */
    public abstract void createFiles(Connector connector, ProjInfo projInfo) throws Exception;

    /**
     * 创建单表类
     *
     * @param connector
     * @param projInfo
     * @param entityInfo
     * @throws Exception
     */
    public abstract void createFilesByOneTable(Connector connector, ProjInfo projInfo,
                                               EntityInfo entityInfo) throws Exception;
}

package com.lp.auto.sdk.po;

/**
 * 数据库连接对象
 */
public class Connector {
    private String dbType; //数据库类型 db2 mysql sqlserver
    private String driver; //数据库驱动
    private String url;  //数据库连接地址
    private String schema; //数据库模式  DB2数据库必须
    private String user; //用户名
    private String password; //密码

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getDbType() {
        return dbType;
    }

    public void setDbType(String dbType) {
        this.dbType = dbType;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

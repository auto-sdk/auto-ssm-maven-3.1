package com.lp.auto.sdk.utils;

/**
 * 常量类
 *
 * @author 兰平
 * @version 1.0
 * @date 2017-6-5
 */
public class Constant {

    /**
     * 警告消息
     */
    public static final String warnMsg = "warning message： ";

    /**
     * 配置文件路径CONFIG_PATH
     */
    public static final String CONFIG_PATH = "config.properties";
    /**
     * CONFIG_PATH key
     */
    public static final String KEY_CREATE_PROJ_URL = "createProjectUrl";

    /**
     * 数据库类型 DB2
     */
    public static final String DB_TYPE_DB2 = "db2";

    /**
     * 数据库类型 MYSQL
     */
    public static final String DB_TYPE_MYSQL = "mysql";

    /**
     * 数据库类型 SQLSERVER
     */
    public static final String DB_TYPE_SQLSERVER = "sqlserver";

    /**
     * key 默认包
     */
    public static final String KEY_DEFAULT_PACKAGE = "defaultPackage";

    /**
     * key basePath
     */
    public static final String KEY_BASE_PATH = "basePath";
    /**
     * key 测试路径
     */
    public static final String KEY_TEST_PATH = "testPath";

    /**
     * key entityPath
     */
    public static final String KEY_ENTITY_PATH = "entityPath";

    /**
     * key utilPath
     */
    public static final String KEY_UTIL_PATH = "utilPath";

    /**
     * key bizPath
     */
    public static final String KEY_BIZ_PATH = "bizPath";

    /**
     * key controllerPath
     */
    public static final String KEY_CONTROLLER_PATH = "controllerPath";

    /**
     * key mappingsPath
     */
    public static final String KEY_MAPPINGS_PATH = "mappingsPath";

    /**
     * key webappPath
     */
    public static final String KEY_WEBAPP_PATH = "webappPath";

    /**
     * key resourcePath
     */
    public static final String KEY_RESOURCE_PATH = "resourcePath";

    /**
     * key serviceImplPath
     */
    public static final String KEY_SERVICE_IMPL_PATH = "serviceImplPath";

    /**
     * key servicePath
     */
    public static final String KEY_SERVICE_PATH = "servicePath";

    /**
     * key mapperPath
     */
    public static final String KEY_MAPPER_PATH = "mapperPath";

    /**
     * key access_base_url
     */
    public static final String KEY_ACCESS_BASE_URL = "access_base_url";

    /**
     * key webxml_path
     */
    public static final String KEY_WEBXML_PATH = "webxml_path";

    /**
     * key webtypeFlag
     */
    public static final String KEY_WEB_TYPE_FLAG = "webtypeFlag";
    
    
    /**
     * key isCreateZIPFile
     */
    public static final String KEY_IS_CREATE_ZIP_FILE = "isCreateZIPFile";

    /**
     * suffix  java
     */
    public static final String FILE_SUFFIX_JAVA = ".java";

    /**
     * suffix  xml
     */
    public static final String FILE_SUFFIX_XML = ".xml";

    /**
     * suffix  zip
     */
    public static final String FILE_SUFFIX_ZIP = ".zip";

    /**
     * suffix properties
     */
    public static final String FILE_SUFFIX_PROPERTIES = ".properties";

    /**
     * 默认资源路径
     */
    public static final String DEFAULT_RESOURCE_PATH = "src/main/resources/";

    /**
     * 默认资源路径
     */
    public static final String DEFAULT_MYBATIS_NAME = "mybatis.xml";

    public static String getFilePath(String projName, PathType pathType, String suffixPath) {
        String rootDir = PropertiesUtil.getConfigValue(Constant.KEY_CREATE_PROJ_URL);
        String srcPath = "";
        if (pathType == PathType.basePath) {
            srcPath = PropertiesUtil.getConfigValue(Constant.KEY_BASE_PATH).replaceAll("/", "\\\\");
        } else if (pathType == PathType.testPath) {
            srcPath = PropertiesUtil.getConfigValue(Constant.KEY_TEST_PATH).replaceAll("/", "\\\\");
        } else if (pathType == PathType.resourcePath) {
            srcPath = PropertiesUtil.getConfigValue(Constant.KEY_RESOURCE_PATH).replaceAll("/", "\\\\");
        } else if (pathType == PathType.webappPath) {
            srcPath = PropertiesUtil.getConfigValue(Constant.KEY_WEBAPP_PATH).replaceAll("/", "\\\\");
        }
        String path = rootDir + projName + "\\" + srcPath + suffixPath;
        return path;
    }

    public enum PathType {
        basePath,
        testPath,
        resourcePath,
        webappPath;
    }
}

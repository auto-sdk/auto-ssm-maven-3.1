package com.lp.auto.sdk.utils;

import java.io.*;
import java.util.Date;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-06-30
 **/
public class FileUtil {
    /**
     * 创建文件
     *
     * @param filePath  文件路径
     * @param content   写入内容
     * @param coverFlag 存在是否覆盖
     */
    public static void createFile(String filePath, String content, boolean coverFlag) throws IOException {
    	File file = null;
    	String createZipFlag = PropertiesUtil.getConfigValue(Constant.KEY_IS_CREATE_ZIP_FILE);
        //如果是生成zip
        if("true".equals(createZipFlag)){
        	file = new File(filePath);
        }else{
        	String rootDir = PropertiesUtil.getConfigValue(Constant.KEY_CREATE_PROJ_URL);
        	filePath = filePath.replace(rootDir, "");
        	file = new File(filePath);
            if (coverFlag == false) {
                if (file.exists()) {
                    System.out.println(Constant.warnMsg + filePath + " 已存在");
                    return;
                }
            }
        }
        
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        System.out.println(DateUtil.formatDate(new Date(), DateUtil.FORMAT_TIMESTAMP) + " " + filePath);
        OutputStream oos = new FileOutputStream(file);
        oos.write(content.getBytes("utf-8"));
        oos.flush();
        oos.close();
    }

    public static void copyFile(String srcFilePath, String destFilePath) {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            File file = new File(srcFilePath);
            File destFile = new File(destFilePath);
            if (!destFile.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            fis = new FileInputStream(file);
            fos = new FileOutputStream(destFile);

            int length = 0;
            while ((length = fis.read()) != -1) {
                fos.write(length);
                fos.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

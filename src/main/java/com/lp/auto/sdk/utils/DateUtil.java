package com.lp.auto.sdk.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-06-30
 **/
public class DateUtil {

    public static final String FORMAT_TIMESTAMP = "yyyy-MM-dd HH:mm:dd.SSS";
    public static final String FORMAT_DATETIME = "yyyy-MM-dd HH:mm:dd";
    public static final String FORMAT_DATE = "yyyy-MM-dd";

    public static String formatDate(Date date, String fomart) {
        SimpleDateFormat sdf = new SimpleDateFormat(fomart);
        return sdf.format(date);
    }

    public static Date parseDate(String dateStr, String fomart) {
        SimpleDateFormat sdf = new SimpleDateFormat(fomart);
        try {
            return sdf.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}

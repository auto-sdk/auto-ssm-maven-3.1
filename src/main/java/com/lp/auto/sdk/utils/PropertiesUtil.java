package com.lp.auto.sdk.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 读取资源配置文件
 */
public class PropertiesUtil extends Properties{

	private static final long serialVersionUID = 1L;
	
	private static PropertiesUtil instance = null; // 这是对外提供的一个实例
	
	public synchronized static PropertiesUtil getInstance() {
		if (instance != null) {
			return instance;
		} else {
			instance = new PropertiesUtil();
			return instance;
		}
	}

	public PropertiesUtil() {
        try {
            InputStream is = PropertiesUtil.class.getResourceAsStream("/"+Constant.CONFIG_PATH);
            this.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
    public static String getConfigValue(String configKey) {
        return PropertiesUtil.getInstance().getProperty(configKey, "utf-8");
    }
}

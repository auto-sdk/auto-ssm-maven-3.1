package com.lp.auto.sdk.utils;

/**
 * 字符串操作类
 *
 * @author 兰平
 * @version 1.0
 * @date 2017-6-5
 */
public class MyStringUtil {

    /**
     * 首字母转小写
     *
     * @param str
     * @return
     */
    public static String toLowerCaseFirstOne(String str) {
        if (Character.isLowerCase(str.charAt(0))) {
            return str;
        } else {
            return (new StringBuilder())
                    .append(Character.toLowerCase(str.charAt(0)))
                    .append(str.substring(1)).toString();
        }
    }

    /**
     * 首字母转大写
     *
     * @param str
     * @return
     */
    public static String toUpperCaseFirstOne(String str) {
        if (Character.isUpperCase(str.charAt(0))) {
            return str;
        } else {
            return (new StringBuilder())
                    .append(Character.toUpperCase(str.charAt(0)))
                    .append(str.substring(1)).toString();
        }
    }

    /**
     * 字符串转换驼峰命名
     *
     * @param str
     * @return
     */
    public static String toUnderscoreToCamelCase(String str) {
        String camelStr = "";
        if (MyStringUtil.isNotNull(str)) {
            String[] items = str.split("_");
            for (int i = 0; i < items.length; i++) {
                if (i == 0) {
                    camelStr += toLowerCaseFirstOne(items[i].toLowerCase());
                } else {
                    camelStr += toUpperCaseFirstOne(items[i].toLowerCase());
                }
            }
        }
        return camelStr;
    }


    /**
     * 对象判非空
     * @param obj
     * @return
     */
    public static boolean isNotNull(Object obj) {
        if(obj!=null && !"".equals(obj.toString())){
            return true;
        }
        return false;
    }

    /**
     * 对象判空
     * @param obj
     * @return
     */
    public static boolean isNull(Object obj) {
        if(obj==null || "".equals(obj.toString())){
            return true;
        }
        return false;
    }

    /**
     * 字符串判空
     *
     * @param str
     * @return
     */
    public static boolean isNull(String str) {
        if (str == null || "".equals(str)) {
            return true;
        }
        return false;
    }

    /**
     * 整数判空
     *
     * @param in
     * @return
     */
    public static boolean isNull(Integer in) {
        if (in == null) {
            return true;
        }
        return false;
    }

    /**
     * 双精度判空
     *
     * @param d
     * @return
     */
    public static boolean isNull(Double d) {
        if (d == null) {
            return true;
        }
        return false;
    }

    /**
     * 单精度判空
     *
     * @param f
     * @return
     */
    public static boolean isNull(Float f) {
        if (f == null) {
            return true;
        }
        return false;
    }

    /**
     * 字节数组判空
     *
     * @param bt
     * @return
     */
    public static boolean isNull(byte[] bt) {
        if (bt == null) {
            return true;
        }
        return false;
    }

    /**
     * 字符串判非空
     *
     * @param str
     * @return
     */
    public static boolean isNotNull(String str) {
        if (str != null && !"".equals(str)) {
            return true;
        }
        return false;
    }

    /**
     * 整数判非空
     *
     * @param in
     * @return
     */
    public static boolean isNotNull(Integer in) {
        if (in != null) {
            return true;
        }
        return false;
    }

    /**
     * 双精度非空
     *
     * @param d
     * @return
     */
    public static boolean isNotNull(Double d) {
        if (d != null) {
            return true;
        }
        return false;
    }

    /**
     * 单精度非空
     *
     * @param f
     * @return
     */
    public static boolean isNotNull(Float f) {
        if (f != null) {
            return true;
        }
        return false;
    }

    /**
     * 字节数组判空
     *
     * @param bt
     * @return
     */
    public static boolean isNotNull(byte[] bt) {
        if (bt != null) {
            return true;
        }
        return false;
    }

}
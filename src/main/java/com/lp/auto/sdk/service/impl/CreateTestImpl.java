package com.lp.auto.sdk.service.impl;

import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;
import com.lp.auto.sdk.service.CreateTest;
import com.lp.auto.sdk.utils.*;

import java.util.Date;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-07-06
 **/
public class CreateTestImpl implements CreateTest {

    public void writeFile(EntityInfo entityInfo, ProjInfo projInfo) throws Exception {
        String entityName = entityInfo.getEntityName();
        String tableComment = entityInfo.getTableComment();

        String upperName = MyStringUtil.toUpperCaseFirstOne(entityName);
        String lowerName = MyStringUtil.toLowerCaseFirstOne(entityName);

        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage + ".test";
        String entityPackName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_ENTITY_PATH);
        String utilPackName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_UTIL_PATH);
        String servicePackName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_SERVICE_PATH);

        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + upperName + "Test" + Constant.FILE_SUFFIX_JAVA;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.testPath, suffixPath);
        StringBuffer sb = new StringBuffer();

        sb.append("package " + packageName + ";\n\n");
        sb.append("import java.util.HashMap;\n");
        sb.append("import java.util.List;\n");
        sb.append("import java.util.Map;\n\n");
        sb.append("import javax.annotation.Resource;\n\n");
        sb.append("import org.junit.Test;\n");
        sb.append("import org.junit.runner.RunWith;\n");
        sb.append("import org.springframework.test.context.ContextConfiguration;\n");
        sb.append("import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;\n\n");
        sb.append("import " + entityPackName + "." + upperName + ";\n");
        sb.append("import " + servicePackName + "." + upperName + "Service;\n");
        sb.append("import " + utilPackName + ".DaoException;\n\n");
        sb.append("/**\n");
        sb.append(" * " + tableComment + "测试类\n");
        sb.append(" * \n");
        sb.append(" * @author " + projInfo.getAuthor() + "\n");
        sb.append(" * @version 1.0\n");
        sb.append(" * @date " + DateUtil.formatDate(new Date(), DateUtil.FORMAT_DATE) + "\n");
        sb.append(" */\n");
        sb.append("@RunWith(SpringJUnit4ClassRunner.class)\n");
        sb.append("@ContextConfiguration(locations={\"classpath:applicationContext.xml\"})\n");
        sb.append("public class " + upperName + "Test {\n\n");
        sb.append("\t@Resource\n");
        sb.append("\tprivate " + upperName + "Service " + lowerName + "Service;\n\n");
        sb.append("\t@Test\n");
        sb.append("\tpublic void test" + upperName + "ListBiz(){\n");
        sb.append("\t\ttry {\n");
        sb.append("\t\t\tMap<String,Object> map=new HashMap<String, Object>();\n");
        sb.append("\t\t\tList<" + upperName + "> list= " + lowerName + "Service.selectAll" + upperName + "List(map);\n");
        sb.append("\t\t\tSystem.out.println(list);\n");
        sb.append("\t\t} catch (DaoException e) {\n");
        sb.append("\t\t\te.printStackTrace();\n");
        sb.append("\t\t}\n");
        sb.append("\t}\n");
        sb.append("}");
        FileUtil.createFile(filePath, sb.toString(), false);
    }
}

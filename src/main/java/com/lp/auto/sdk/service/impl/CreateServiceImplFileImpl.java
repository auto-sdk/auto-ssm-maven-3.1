package com.lp.auto.sdk.service.impl;

import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;
import com.lp.auto.sdk.service.CreateServiceImpl;
import com.lp.auto.sdk.utils.*;

import java.util.Date;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-07-06
 **/
public class CreateServiceImplFileImpl implements CreateServiceImpl {

    /**
     * 创建接口实现文件
     *
     * @param entityInfo 实体名
     * @param projInfo   项目信息
     * @return
     * @throws Exception
     */
    public void writeFile(EntityInfo entityInfo, ProjInfo projInfo, String dbType) throws Exception {

        String entityName = entityInfo.getEntityName();
        String tableComment = entityInfo.getTableComment();
        String primaryKey = entityInfo.getPrimaryKey();
        String upPrimaryKey = MyStringUtil.toUpperCaseFirstOne(primaryKey);
        String primaryKeyType = entityInfo.getPrimaryKeyType();

        String upperName = MyStringUtil.toUpperCaseFirstOne(entityName);
        String lowerName = MyStringUtil.toLowerCaseFirstOne(entityName);

        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_SERVICE_IMPL_PATH);
        String entityPackName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_ENTITY_PATH);
        String utilPackName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_UTIL_PATH);
        String mapperPackName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_MAPPER_PATH);
        String servicePackName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_SERVICE_PATH);

        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + upperName + "ServiceImpl" + Constant.FILE_SUFFIX_JAVA;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);
        StringBuffer sb = new StringBuffer();

        sb.append("package ");
        sb.append(packageName);
        sb.append(";\n\n");

        sb.append("import java.util.List;\n");
        sb.append("import java.util.Map;\n\n");
        sb.append("import org.springframework.beans.factory.annotation.Autowired;\n");
        sb.append("import org.springframework.stereotype.Service;\n\n");

        sb.append("import "+entityPackName+"." + upperName + ";\n");
        sb.append("import "+mapperPackName+"." + upperName + "Mapper;\n");
        sb.append("import "+servicePackName+"." + upperName+"Service;\n");
        sb.append("import "+utilPackName+".DaoException;\n");
        sb.append("import "+utilPackName+".ValidateException;\n");
        sb.append("import "+utilPackName+".ConstantUtil;\n\n");

        sb.append("/**\n");
        sb.append(" * " + tableComment + "实现类\n");
        sb.append(" * \n");
        sb.append(" * @author " + projInfo.getAuthor() + "\n");
        sb.append(" * @version 1.0\n");
        sb.append(" * @date " + DateUtil.formatDate(new Date(), DateUtil.FORMAT_DATE) + "\n");
        sb.append(" */\n");
        sb.append("@Service\n");
        sb.append("public class " + upperName
                + "ServiceImpl implements "
                + upperName + "Service {\n\n");

        sb.append("\t@Autowired\n");
        sb.append("\tprivate " + upperName + "Mapper " + lowerName + "Mapper;\n");

        sb.append("\t/**\n");
        sb.append("\t * 新增\n");
        sb.append("\t * \n");
        sb.append("\t * @param " + lowerName + "\n");
        sb.append("\t * @return\n");
        sb.append("\t * @throws DaoException\n");
        sb.append("\t */\n");
        sb.append("\tpublic int insert" + upperName
                + "(" + upperName + " " + lowerName + ") throws DaoException {\n");
        sb.append("\t\tint result = 0;\n");
        sb.append("\t\ttry {\n");
        sb.append("\t\t\tresult = " + lowerName + "Mapper.insert" + upperName + "(" + lowerName + ");\n");
        sb.append("\t\t} catch (Exception e){\n");
        sb.append("\t\t\tthrow new DaoException(e, ConstantUtil.MSG_ADD_ERROR);\n");
        sb.append("\t\t}\n");
        sb.append("\t\treturn result;\n");
        sb.append("\t}\n\n");

        sb.append("\t/**\n");
        sb.append("\t * 修改\n");
        sb.append("\t * \n");
        sb.append("\t * @param " + lowerName + "\n");
        sb.append("\t * @return\n");
        sb.append("\t * @throws DaoException\n");
        sb.append("\t */\n");
        sb.append("\tpublic int update" + upperName
                + "(" + upperName + " " + lowerName + ") throws DaoException {\n");
        sb.append("\t\tint result = 0;\n");
        sb.append("\t\ttry {\n");
        sb.append("\t\t\tresult = " + lowerName + "Mapper.update" + upperName + "(" + lowerName + ");\n");
        sb.append("\t\t} catch (Exception e){\n");
        sb.append("\t\t\tthrow new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);\n");
        sb.append("\t\t}\n");
        sb.append("\t\treturn result;\n");
        sb.append("\t}\n\n");

        sb.append("\t/**\n");
        sb.append("\t * 删除\n");
        sb.append("\t * \n");
        sb.append("\t * @param map\n");
        sb.append("\t * @return\n");
        sb.append("\t * @throws DaoException\n");
        sb.append("\t */\n");
        sb.append("\tpublic int delete" + upperName
                + "(Map<String, Object> map) throws DaoException {\n");
        sb.append("\t\tint result = 0;\n");
        sb.append("\t\ttry {\n");
        sb.append("\t\t\tresult = " + lowerName + "Mapper.delete" + upperName + "(map);\n");
        sb.append("\t\t} catch (Exception e){\n");
        sb.append("\t\t\tthrow new DaoException(e, ConstantUtil.MSG_DEL_ERROR);\n");
        sb.append("\t\t}\n");
        sb.append("\t\treturn result;\n");
        sb.append("\t}\n\n");

        sb.append("\t/**\n");
        sb.append("\t * 查询单个\n");
        sb.append("\t * \n");
        sb.append("\t * @param " + primaryKey + "\n");
        sb.append("\t * @return\n");
        sb.append("\t * @throws DaoException\n");
        sb.append("\t */\n");
        sb.append("\tpublic " + upperName + " select" + upperName
                + "By" + upPrimaryKey + "(" + primaryKeyType + " " + primaryKey + ") throws DaoException {\n");
        sb.append("\t\t"+upperName+" "+lowerName+" = null;\n");
        sb.append("\t\ttry {\n");
        sb.append("\t\t\t"+lowerName+" = " + lowerName + "Mapper.select" + upperName
                + "By" + upPrimaryKey + "(" + primaryKey + ");\n");
        sb.append("\t\t} catch (Exception e){\n");
        sb.append("\t\t\tthrow new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);\n");
        sb.append("\t\t}\n");
        sb.append("\t\treturn "+lowerName+";\n");
        sb.append("\t}\n\n");

        sb.append("\t/**\n");
        sb.append("\t * 分页查询\n");
        sb.append("\t * \n");
        sb.append("\t * @param map\n");
        sb.append("\t * @return\n");
        sb.append("\t * @throws DaoException\n");
        sb.append("\t * @throws ValidateException\n");
        sb.append("\t */\n");
        sb.append("\tpublic List<" + upperName + "> select" + upperName
                + "List(Map<String, Object> map) throws DaoException, ValidateException {\n");

        sb.append("\t\tList<"+upperName+"> "+lowerName+"s = null;\n\n");

        if (dbType.equals(Constant.DB_TYPE_SQLSERVER) || dbType.equals(Constant.DB_TYPE_DB2)) {
            sb.append("\t\tif (map == null || !map.containsKey(\"pageIndex\") || !map.containsKey(\"pageSize\")) {\n");
            sb.append("\t\t\tthrow new ValidateException(ConstantUtil.MSG_PAGE_DATA_ERROR);\n");
            sb.append("\t\t}\n\n");
        } else if (dbType.equals(Constant.DB_TYPE_MYSQL)) {
            sb.append("\t\ttry {\n");
            sb.append("\t\t\tif (map != null && map.containsKey(\"pageIndex\") && map.containsKey(\"pageSize\")) {\n");
            sb.append("\t\t\t\tInteger index = Integer.parseInt(map.get(\"pageIndex\").toString());\n");
            sb.append("\t\t\t\tInteger size = Integer.parseInt(map.get(\"pageSize\").toString());\n");
            sb.append("\t\t\t\tInteger count = (index-1)*size;\n");
            sb.append("\t\t\t\tmap.put(\"countIndex\", count);\n");
            sb.append("\t\t\t}\n\n");
            sb.append("\t\t} catch (Exception e){\n");
            sb.append("\t\t\tthrow new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);\n");
            sb.append("\t\t}\n\n");
        }
        sb.append("\t\ttry {\n");
        sb.append("\t\t\t"+lowerName+"s = " + lowerName + "Mapper.select" + upperName+ "List(map);\n");
        sb.append("\t\t} catch (Exception e){\n");
        sb.append("\t\t\tthrow new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);\n");
        sb.append("\t\t}\n\n");

        sb.append("\t\treturn "+lowerName+"s;\n");
        sb.append("\t}\n\n");

        sb.append("\t/**\n");
        sb.append("\t * 查询所有\n");
        sb.append("\t * \n");
        sb.append("\t * @param map\n");
        sb.append("\t * @return\n");
        sb.append("\t * @throws DaoException\n");
        sb.append("\t */\n");
        sb.append("\tpublic List<" + upperName + "> selectAll" + upperName
                + "List(Map<String, Object> map) throws DaoException {\n");
        sb.append("\t\tList<"+upperName+"> "+lowerName+"s = null;\n");
        sb.append("\t\ttry {\n");
        sb.append("\t\t\t"+lowerName+"s = " + lowerName + "Mapper.selectAll" + upperName+ "List(map);\n");
        sb.append("\t\t} catch (Exception e){\n");
        sb.append("\t\t\tthrow new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);\n");
        sb.append("\t\t}\n");
        sb.append("\t\treturn "+lowerName+"s;\n");
        sb.append("\t}\n\n");

        sb.append("\t/**\n");
        sb.append("\t * 查询总数\n");
        sb.append("\t * \n");
        sb.append("\t * @param map\n");
        sb.append("\t * @return\n");
        sb.append("\t * @throws DaoException\n");
        sb.append("\t */\n");
        sb.append("\tpublic int select" + upperName + "ListCount(Map<String, Object> map) throws DaoException {\n");
        sb.append("\t\tint count = 0;\n");
        sb.append("\t\ttry {\n");
        sb.append("\t\t\tObject obj = " + lowerName + "Mapper.select" + upperName + "ListCount(map);\n");
        sb.append("\t\t\tif(obj != null){\n");
        sb.append("\t\t\t\tcount = Integer.parseInt(obj.toString());\n");
        sb.append("\t\t\t}\n");
        sb.append("\t\t} catch (Exception e){\n");
        sb.append("\t\t\tthrow new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);\n");
        sb.append("\t\t}\n");
        sb.append("\t\treturn count;\n");
        sb.append("\t}\n\n");

        sb.append("}");
        FileUtil.createFile(filePath, sb.toString(), false);

    }

}

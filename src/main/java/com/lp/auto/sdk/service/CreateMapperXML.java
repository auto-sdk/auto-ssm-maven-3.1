package com.lp.auto.sdk.service;

import com.lp.auto.sdk.po.Connector;
import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-07-06
 **/
public interface CreateMapperXML {
    /**
     * 写文件
     *
     * @param entityInfo 实体信息
     * @param projInfo   项目信息
     */
    public void writeFile(EntityInfo entityInfo, ProjInfo projInfo, Connector connector) throws Exception;
}

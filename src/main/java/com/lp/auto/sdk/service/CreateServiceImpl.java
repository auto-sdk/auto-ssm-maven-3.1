package com.lp.auto.sdk.service;

import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-07-06
 **/
public interface CreateServiceImpl {

    /**
     * 创建接口实现文件
     *
     * @param entityInfo 实体名
     * @param projInfo   项目信息
     * @return
     * @throws Exception
     */
    public void writeFile(EntityInfo entityInfo, ProjInfo projInfo, String dbTypejInfo) throws Exception;
}

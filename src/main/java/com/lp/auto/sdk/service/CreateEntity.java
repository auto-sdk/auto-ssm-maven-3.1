package com.lp.auto.sdk.service;

import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;

import java.sql.ResultSet;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-07-06
 **/
public interface CreateEntity {

    /**
     * 创建Entity
     *
     * @param resultSet  结果集
     * @param entityInfo 实体信息
     * @param projInfo   项目信息
     * @return 返回实体信息
     * @throws Exception
     */
    public EntityInfo writeFile(ResultSet resultSet, EntityInfo entityInfo,
                                ProjInfo projInfo) throws Exception;
}

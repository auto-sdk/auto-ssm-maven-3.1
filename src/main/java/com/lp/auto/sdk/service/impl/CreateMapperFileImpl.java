package com.lp.auto.sdk.service.impl;

import java.util.Date;

import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;
import com.lp.auto.sdk.service.CreateMapper;
import com.lp.auto.sdk.utils.Constant;
import com.lp.auto.sdk.utils.DateUtil;
import com.lp.auto.sdk.utils.FileUtil;
import com.lp.auto.sdk.utils.MyStringUtil;
import com.lp.auto.sdk.utils.PropertiesUtil;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-09-10
 **/
public class CreateMapperFileImpl implements CreateMapper {

    public void writeFile(EntityInfo entityInfo, ProjInfo projInfo) throws Exception {
        String entityName = entityInfo.getEntityName();
        String tableComment = entityInfo.getTableComment();
        String primaryKey = entityInfo.getPrimaryKey();
        String upPrimaryKey = MyStringUtil.toUpperCaseFirstOne(primaryKey);
        String primaryKeyType = entityInfo.getPrimaryKeyType();

        String upperName = MyStringUtil.toUpperCaseFirstOne(entityName);
        String lowerName = MyStringUtil.toLowerCaseFirstOne(entityName);

        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_MAPPER_PATH);
        String entityPackName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_ENTITY_PATH);

        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + upperName + "Mapper" + Constant.FILE_SUFFIX_JAVA;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);
        StringBuffer sb = new StringBuffer();

        sb.append("package ");
        sb.append(packageName);
        sb.append(";\n\n");

        sb.append("import java.util.List;\n");
        sb.append("import java.util.Map;\n\n");

        sb.append("import ");
        sb.append(entityPackName);
        sb.append("." + upperName);
        sb.append(";\n\n");
        sb.append("/**\n");
        sb.append(" * " + tableComment + "接口\n");
        sb.append(" * \n");
        sb.append(" * @author " + projInfo.getAuthor() + "\n");
        sb.append(" * @version 1.0\n");
        sb.append(" * @date " + DateUtil.formatDate(new Date(), DateUtil.FORMAT_DATE) + "\n");
        sb.append(" */\n");
        sb.append("public interface " + upperName + "Mapper {\n\n");
        sb.append("\t/**\n");
        sb.append("\t * 新增\n");
        sb.append("\t * \n");
        sb.append("\t * @param " + lowerName + "\n");
        sb.append("\t * @return\n");
        sb.append("\t * @throws Exception\n");
        sb.append("\t */\n");
        sb.append("\tpublic int insert" + upperName + "(" + upperName + " " + lowerName
                + ") throws Exception;\n\n");
        sb.append("\t/**\n");
        sb.append("\t * 修改\n");
        sb.append("\t * \n");
        sb.append("\t * @param " + lowerName + "\n");
        sb.append("\t * @return\n");
        sb.append("\t * @throws Exception\n");
        sb.append("\t */\n");
        sb.append("\tpublic int update" + upperName + "(" + upperName + " " + lowerName
                + ") throws Exception;\n\n");
        sb.append("\t/**\n");
        sb.append("\t * 删除\n");
        sb.append("\t * \n");
        sb.append("\t * @param map\n");
        sb.append("\t * @return\n");
        sb.append("\t * @throws Exception\n");
        sb.append("\t */\n");
        sb.append("\tpublic int delete" + upperName
                + "(Map<String, Object> map) throws Exception;\n\n");
        sb.append("\t/**\n");
        sb.append("\t * 查询单个\n");
        sb.append("\t * \n");
        sb.append("\t * @param " + primaryKey + "\n");
        sb.append("\t * @return\n");
        sb.append("\t */\n");
        sb.append("\tpublic " + upperName
                + " select" + upperName
                + "By" + upPrimaryKey + "(" + primaryKeyType + " " + primaryKey + ") throws Exception;\n\n");
        sb.append("\t/**\n");
        sb.append("\t * 分页查询\n");
        sb.append("\t * \n");
        sb.append("\t * @param map\n");
        sb.append("\t * @return\n");
        sb.append("\t */\n");
        sb.append("\tpublic List<" + upperName
                + "> select" + upperName
                + "List(Map<String, Object> map) throws Exception;\n\n");
        sb.append("\t/**\n");
        sb.append("\t * 查询所有\n");
        sb.append("\t * \n");
        sb.append("\t * @param map\n");
        sb.append("\t * @return\n");
        sb.append("\t */\n");
        sb.append("\tpublic List<" + upperName
                + "> selectAll" + upperName
                + "List(Map<String, Object> map) throws Exception;\n\n");
        sb.append("\t/**\n");
        sb.append("\t * 查询总数\n");
        sb.append("\t * \n");
        sb.append("\t * @param map\n");
        sb.append("\t * @return\n");
        sb.append("\t */\n");
        sb.append("\tpublic int select" + upperName
                + "ListCount(Map<String, Object> map) throws Exception;\n\n");
        sb.append("}");

        FileUtil.createFile(filePath, sb.toString(), false);
    }
}

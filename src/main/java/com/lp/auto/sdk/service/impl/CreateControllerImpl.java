package com.lp.auto.sdk.service.impl;

import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;
import com.lp.auto.sdk.service.CreateController;
import com.lp.auto.sdk.utils.*;

import java.util.Date;
import java.util.List;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-07-06
 **/
public class CreateControllerImpl implements CreateController {

    /**
     * 创建Controller文件
     *
     * @param entityInfo 实体信息
     * @param projInfo   项目信息
     * @throws Exception
     */
    public void writeFile(EntityInfo entityInfo, ProjInfo projInfo) throws Exception {
        String entityName = entityInfo.getEntityName();
        String tableComment = entityInfo.getTableComment();
        String primaryKey = entityInfo.getPrimaryKey();
        String primaryKeyType = entityInfo.getPrimaryKeyType();
        List<String> colnames = entityInfo.getColnames();

        String upperName = MyStringUtil.toUpperCaseFirstOne(entityName);
        String lowerName = MyStringUtil.toLowerCaseFirstOne(entityName);

        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_CONTROLLER_PATH);
        String entityPackName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_ENTITY_PATH);
        String utilPackName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_UTIL_PATH);
        String bizPackName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_BIZ_PATH);

        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + upperName + "Controller" + Constant.FILE_SUFFIX_JAVA;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);
        StringBuffer sb = new StringBuffer();
        sb.append("package "+packageName+";\n\n");
        sb.append("import java.util.List;\n");
        sb.append("import java.util.Map;\n\n");
        sb.append("import javax.annotation.Resource;\n");
        sb.append("import javax.servlet.http.HttpServletRequest;\n");
        sb.append("import javax.servlet.http.HttpServletResponse;\n\n");
        sb.append("import org.springframework.stereotype.Controller;\n");
        sb.append("import org.springframework.web.bind.annotation.ResponseBody;\n");
        sb.append("import org.springframework.web.bind.annotation.RequestMapping;\n\n");

        sb.append("import "+entityPackName+"." + upperName + ";\n");
        sb.append("import " + bizPackName + "." + upperName + "Biz;\n");
        sb.append("import " + utilPackName + ".ConstantUtil;\n");
        sb.append("import " + utilPackName + ".DaoException;\n");
        sb.append("import " + utilPackName + ".ValidateException;\n");
        sb.append("import " + utilPackName + ".ResultEntity;\n\n");

        sb.append("/**\n");
        sb.append(" * " + tableComment + "控制器\n");
        sb.append(" * \n");
        sb.append(" * @author " + projInfo.getAuthor() + "\n");
        sb.append(" * @version 1.0\n");
        sb.append(" * @date " + DateUtil.formatDate(new Date(), DateUtil.FORMAT_DATE) + "\n");
        sb.append(" */\n");
        sb.append("@Controller\n");
        sb.append("public class " + upperName + "Controller extends BaseController {\n\n");

        sb.append("\t@Resource\n");
        sb.append("\tprivate " + upperName + "Biz " + lowerName + "Biz;\n\n");

        sb.append(selectEntityList(entityName, colnames));
        sb.append(selectEntityById(entityName, primaryKeyType, primaryKey));
        sb.append(insertEntity(entityName));
        sb.append(updateEntity(entityName));
        sb.append(deleteEntity(entityName, primaryKey));
        sb.append("}");

        FileUtil.createFile(filePath, sb.toString(), false);
    }

    /**
     * 查询集合
     *
     * @param entityName
     * @param list
     * @return
     */
    private String selectEntityList(String entityName, List<String> list) {
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityName);
        String lowerName = MyStringUtil.toLowerCaseFirstOne(entityName);
        String baseUrl = PropertiesUtil.getConfigValue(Constant.KEY_ACCESS_BASE_URL);
        StringBuffer sb = new StringBuffer();
        sb.append("\t/**\n");
        sb.append("\t * 查询" + upperName + "集合\n");
        sb.append("\t */\n");
        sb.append("\t@ResponseBody\n");
        sb.append("\t@RequestMapping(value = \"/" + baseUrl + lowerName + "List\", produces = \"application/json;charset=UTF-8\")\n");
        sb.append("\tpublic String " + lowerName + "List(HttpServletRequest request, HttpServletResponse response) {\n");
        sb.append("\t\tResultEntity<List<" + upperName + ">> resultEntity = null;\n");
        sb.append("\t\ttry {\n");
        sb.append("\t\t\tMap<String,Object> map=initRequestMap(request);\n");
        sb.append("\t\t\tresultEntity = " + lowerName + "Biz." + lowerName + "List(map);\n");
        sb.append("\t\t} catch (DaoException e) {\n");
        sb.append("\t\t\tresultEntity = new ResultEntity<List<" + upperName + ">>(ConstantUtil.CODE_500, e.getMessage());\n");
        sb.append("\t\t\te.printStackTrace();\n");
        sb.append("\t\t} catch (ValidateException e) {\n");
        sb.append("\t\t\tresultEntity = new ResultEntity<List<" + upperName + ">>(ConstantUtil.CODE_500, e.getMessage());\n");
        sb.append("\t\t\te.printStackTrace();\n");
        sb.append("\t\t}\n");
        sb.append("\t\treturn resultEntity.toString();\n");
        sb.append("\t}\n\n");
        return sb.toString();
    }

    /**
     * 查询单个
     *
     * @param entityName
     * @param primaryKey
     * @return
     */
    private String selectEntityById(String entityName, String primaryKeyType, String primaryKey) {
        String baseUrl = PropertiesUtil.getConfigValue(Constant.KEY_ACCESS_BASE_URL);
        String upPrimaryKey = MyStringUtil.toUpperCaseFirstOne(primaryKey);
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityName);
        String lowerName = MyStringUtil.toLowerCaseFirstOne(entityName);
        StringBuffer sb = new StringBuffer();
        sb.append("\t/**\n");
        sb.append("\t * 根据" + primaryKey + "查询" + upperName + "\n");
        sb.append("\t */\n");
        sb.append("\t@ResponseBody\n");
        sb.append("\t@RequestMapping(value = \"/" + baseUrl + lowerName + "By" + upPrimaryKey + "\", produces = \"application/json;charset=UTF-8\")\n");
        sb.append("\tpublic String " + lowerName + "By" + upPrimaryKey + "(HttpServletRequest request, HttpServletResponse response) {\n");
        sb.append("\t\tResultEntity<" + upperName + "> resultEntity = null;\n");
        sb.append("\t\ttry {\n");
        sb.append("\t\t\tString " + primaryKey + " = request.getParameter(\"" + primaryKey + "\");\n");
        sb.append("\t\t\tresultEntity = " + lowerName + "Biz." + lowerName + "By" + upPrimaryKey + "(" + primaryKey + ");\n");
        sb.append("\t\t} catch (DaoException e) {\n");
        sb.append("\t\t\tresultEntity = new ResultEntity<" + upperName + ">(ConstantUtil.CODE_500, e.getMessage());\n");
        sb.append("\t\t\te.printStackTrace();\n");
        sb.append("\t\t}\n");
        sb.append("\t\treturn resultEntity.toString();\n");
        sb.append("\t}\n\n");
        return sb.toString();
    }

    /**
     * 新增
     *
     * @param entityName
     * @return
     */
    private String insertEntity(String entityName) {
        String baseUrl = PropertiesUtil.getConfigValue(Constant.KEY_ACCESS_BASE_URL);
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityName);
        String lowerName = MyStringUtil.toLowerCaseFirstOne(entityName);
        StringBuffer sb = new StringBuffer();
        sb.append("\t/**\n");
        sb.append("\t * 新增" + upperName + "\n");
        sb.append("\t */\n");
        sb.append("\t@ResponseBody\n");
        sb.append("\t@RequestMapping(value = \"/" + baseUrl + "add" + upperName + "\", produces = \"application/json;charset=UTF-8\")\n");
        sb.append("\tpublic String add" + upperName + "(" + upperName + " " + lowerName + ", HttpServletRequest request, HttpServletResponse response) {\n");
        sb.append("\t\tResultEntity<" + upperName + "> resultEntity = null;\n");
        sb.append("\t\ttry {\n");
        sb.append("\t\t\tresultEntity = " + lowerName + "Biz.insert" + upperName + "(" + lowerName + ");\n");
        sb.append("\t\t} catch (DaoException e) {\n");
        sb.append("\t\t\tresultEntity = new ResultEntity<" + upperName + ">(ConstantUtil.CODE_500, e.getMessage());\n");
        sb.append("\t\t\te.printStackTrace();\n");
        sb.append("\t\t}\n");
        sb.append("\t\treturn resultEntity.toString();\n");
        sb.append("\t}\n\n");
        return sb.toString();
    }


    /**
     * 修改
     *
     * @param entityName
     * @return
     */
    private String updateEntity(String entityName) {
        String baseUrl = PropertiesUtil.getConfigValue(Constant.KEY_ACCESS_BASE_URL);
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityName);
        String lowerName = MyStringUtil.toLowerCaseFirstOne(entityName);
        StringBuffer sb = new StringBuffer();
        sb.append("\t/**\n");
        sb.append("\t * 修改" + upperName + "\n");
        sb.append("\t */\n");
        sb.append("\t@ResponseBody\n");
        sb.append("\t@RequestMapping(value = \"/" + baseUrl + "update" + upperName + "\", produces = \"application/json;charset=UTF-8\")\n");
        sb.append("\tpublic String update" + upperName + "(" + upperName + " " + lowerName + ", HttpServletRequest request, HttpServletResponse response) {\n");
        sb.append("\t\tResultEntity<" + upperName + "> resultEntity = null;\n");
        sb.append("\t\ttry {\n");
        sb.append("\t\t\tresultEntity = " + lowerName + "Biz.update" + upperName + "(" + lowerName + ");\n");
        sb.append("\t\t} catch (DaoException e) {\n");
        sb.append("\t\t\tresultEntity = new ResultEntity<" + upperName + ">(ConstantUtil.CODE_500, e.getMessage());\n");
        sb.append("\t\t\te.printStackTrace();\n");
        sb.append("\t\t}\n");
        sb.append("\t\treturn resultEntity.toString();\n");
        sb.append("\t}\n\n");
        return sb.toString();
    }


    /**
     * 删除
     *
     * @param entityName
     * @return
     */
    private String deleteEntity(String entityName, String primaryKey) {
        String baseUrl = PropertiesUtil.getConfigValue(Constant.KEY_ACCESS_BASE_URL);
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityName);
        String lowerName = MyStringUtil.toLowerCaseFirstOne(entityName);
        StringBuffer sb = new StringBuffer();
        sb.append("\t/**\n");
        sb.append("\t * 删除" + upperName + "\n");
        sb.append("\t */\n");
        sb.append("\t@ResponseBody\n");
        sb.append("\t@RequestMapping(value = \"/" + baseUrl + "delete" + upperName + "\", produces = \"application/json;charset=UTF-8\")\n");
        sb.append("\tpublic String delete" + upperName + "(HttpServletRequest request, HttpServletResponse response) {\n");
        sb.append("\t\tResultEntity<" + upperName + "> resultEntity = null;\n");
        sb.append("\t\ttry {\n");
        sb.append("\t\t\tMap<String,Object> map = this.initRequestMap(request);\n");
        sb.append("\t\t\tresultEntity = " + lowerName + "Biz.delete" + upperName + "(map);\n");
        sb.append("\t\t} catch (DaoException e) {\n");
        sb.append("\t\t\tresultEntity = new ResultEntity<" + upperName + ">(ConstantUtil.CODE_500, e.getMessage());\n");
        sb.append("\t\t\te.printStackTrace();\n");
        sb.append("\t\t}\n");
        sb.append("\t\treturn resultEntity.toString();\n");
        sb.append("\t}\n\n");
        return sb.toString();
    }
}

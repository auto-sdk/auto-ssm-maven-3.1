package com.lp.auto.sdk.service;

import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-07-06
 **/
public interface CreateMapper {
    /**
     * 写文件
     *
     * @param entityInfo
     */
    public void writeFile(EntityInfo entityInfo, ProjInfo projInfo) throws Exception;
}

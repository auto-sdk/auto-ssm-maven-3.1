package com.lp.auto.sdk.service.impl;

import java.util.Date;

import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;
import com.lp.auto.sdk.service.CreateBiz;
import com.lp.auto.sdk.utils.Constant;
import com.lp.auto.sdk.utils.DateUtil;
import com.lp.auto.sdk.utils.FileUtil;
import com.lp.auto.sdk.utils.MyStringUtil;
import com.lp.auto.sdk.utils.PropertiesUtil;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-07-06
 **/
public class CreateBizImpl implements CreateBiz {

    /**
     * 创建BIZ文件
     *
     * @param entityInfo 实体信息
     * @param projInfo   项目信息
     * @throws Exception
     */
    public void writeFile(EntityInfo entityInfo, ProjInfo projInfo) throws Exception {

        String entityName = entityInfo.getEntityName();
        String tableComment = entityInfo.getTableComment();
        String primaryKey = entityInfo.getPrimaryKey();
        String primaryKeyType = entityInfo.getPrimaryKeyType();

        String lowerName = MyStringUtil.toLowerCaseFirstOne(entityName);
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityName);

        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_BIZ_PATH);
        String entityPackName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_ENTITY_PATH);
        String utilPackName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_UTIL_PATH);
        String servicePackName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_SERVICE_PATH);

        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + upperName + "Biz" + Constant.FILE_SUFFIX_JAVA;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);

        StringBuffer sb = new StringBuffer();
        sb.append("package " + packageName + ";\n\n");
        sb.append("import java.util.List;\n");
        if("String".equals(primaryKeyType)){
        	sb.append("import java.util.UUID;\n");
        }
        sb.append("import java.util.Map;\n\n");
        
        sb.append("import javax.annotation.Resource;\n\n");
        sb.append("import org.springframework.stereotype.Service;\n");
        sb.append("import " + entityPackName + "." + upperName + ";\n");
        sb.append("import " + servicePackName + "." + upperName + "Service;\n");
        sb.append("import " + utilPackName + ".ConstantUtil;\n");
        sb.append("import " + utilPackName + ".StringUtil;\n");
        sb.append("import " + utilPackName + ".RegexUtil;\n");
        sb.append("import " + utilPackName + ".DaoException;\n");
        sb.append("import " + utilPackName + ".ValidateException;\n");
        sb.append("import " + utilPackName + ".ResultEntity;\n\n");
        sb.append("/**\n");
        sb.append(" * " + tableComment + "业务处理类\n");
        sb.append(" * \n");
        sb.append(" * @author " + projInfo.getAuthor() + "\n");
        sb.append(" * @version 1.0\n");
        sb.append(" * @date " + DateUtil.formatDate(new Date(), DateUtil.FORMAT_DATE) + "\n");
        sb.append(" */\n");
        sb.append("@Service\n");
        sb.append("public class " + upperName + "Biz {\n\n");

        sb.append("\t@Resource\n");
        sb.append("\tprivate " + upperName + "Service " + lowerName + "Service;\n\n");

        sb.append(selectEntityList(entityName));
        sb.append(selectAllEntityList(entityName));
        sb.append(selectEntityById(entityName, primaryKeyType, primaryKey));
        sb.append(insertEntity(entityName, primaryKeyType, primaryKey));
        sb.append(updateEntity(entityName));
        sb.append(deleteEntity(entityName,primaryKey));
        sb.append("}");

        FileUtil.createFile(filePath, sb.toString(), false);
    }

    /**
     * 查询集合
     *
     * @param entityName
     * @return
     */
    private String selectEntityList(String entityName) {
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityName);
        String lowerName = MyStringUtil.toLowerCaseFirstOne(entityName);
        StringBuffer sb = new StringBuffer();
        sb.append("\t/**\n");
        sb.append("\t * 查询" + upperName + "集合\n");
        sb.append("\t */\n");
        sb.append("\tpublic ResultEntity<List<" + upperName + ">> " + lowerName + "List(Map<String, Object> map) throws DaoException, ValidateException{\n");
        sb.append("\t\tList<" + upperName + "> " + lowerName + "s = " + lowerName + "Service.select" + upperName + "List(map);\n");
        sb.append("\t\tint total = " + lowerName + "Service.select" + upperName + "ListCount(map);\n");
        sb.append("\t\treturn new ResultEntity<List<" + upperName + ">>(ConstantUtil.CODE_200, " + lowerName + "s, total);\n");
        sb.append("\t}\n\n");
        return sb.toString();
    }
    
    
    /**
     * 查询所有集合
     *
     * @param entityName
     * @return
     */
    private String selectAllEntityList(String entityName) {
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityName);
        String lowerName = MyStringUtil.toLowerCaseFirstOne(entityName);
        StringBuffer sb = new StringBuffer();
        sb.append("\t/**\n");
        sb.append("\t * 查询所有" + upperName + "集合\n");
        sb.append("\t */\n");
        sb.append("\tpublic List<" + upperName + "> " + lowerName + "AllList(Map<String, Object> map) throws DaoException {\n");
        sb.append("\t\treturn " + lowerName + "Service.selectAll" + upperName + "List(map);\n");
        sb.append("\t}\n\n");
        return sb.toString();
    }
    

    /**
     * 查询单个
     *
     * @param entityName
     * @param primaryKey
     * @return
     */
    private String selectEntityById(String entityName, String primaryKeyType, String primaryKey) {
        String upPrimaryKey = MyStringUtil.toUpperCaseFirstOne(primaryKey);
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityName);
        String lowerName = MyStringUtil.toLowerCaseFirstOne(entityName);
        StringBuffer sb = new StringBuffer();
        sb.append("\t/**\n");
        sb.append("\t * 根据" + primaryKey + "查询" + upperName + "\n");
        sb.append("\t */\n");
        sb.append("\tpublic ResultEntity<" + upperName + "> " + lowerName + "By" + upPrimaryKey + "(String " + primaryKey + ") throws DaoException{\n");
        sb.append("\t\tif (StringUtil.isNotNull(" + primaryKey + ")) {\n");
        if ("Integer".equals(primaryKeyType)) {
            sb.append("\t\t\t" + upperName + " " + lowerName + " = " + lowerName +
                    "Service.select" + upperName + "By" + upPrimaryKey + "(Integer.parseInt(" + primaryKey
                    + "));\n");
        } else {
            sb.append("\t\t\t" + upperName + " " + lowerName + " = " +
                    lowerName + "Service.select" + upperName + "By" + upPrimaryKey + "(" + primaryKey + ");\n");
        }
        sb.append("\t\t\treturn new ResultEntity<" + upperName + ">(ConstantUtil.CODE_200, " + lowerName + ");\n");
        sb.append("\t\t} else {\n");
        sb.append("\t\t\treturn new ResultEntity<" + upperName + ">(ConstantUtil.CODE_404, ConstantUtil.MSG_404);\n");
        sb.append("\t\t}\n");
        sb.append("\t}\n\n");
        return sb.toString();
    }

    /**
     * 新增
     *
     * @param entityName
     * @param primaryKey 
     * @param primaryKeyType 
     * @return
     */
    private String insertEntity(String entityName, String primaryKeyType, String primaryKey) {
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityName);
        String lowerName = MyStringUtil.toLowerCaseFirstOne(entityName);
        String upPrimaryKey= MyStringUtil.toUpperCaseFirstOne(primaryKey);
        StringBuffer sb = new StringBuffer();
        sb.append("\t/**\n");
        sb.append("\t * 新增" + upperName + "\n");
        sb.append("\t */\n");
        sb.append("\tpublic ResultEntity<" + upperName + "> insert" + upperName + "(" + upperName + " " + lowerName + ") throws DaoException{\n");
        if("String".equals(primaryKeyType)){
        	sb.append("\t\t"+lowerName+".set"+upPrimaryKey+"(UUID.randomUUID().toString());"+"\n");
        }
        sb.append("\t\t" + lowerName + "Service.insert" + upperName + "(" + lowerName + ");\n");
        sb.append("\t\treturn new ResultEntity<" + upperName + ">(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);\n");
        sb.append("\t}\n\n");
        return sb.toString();
    }


    /**
     * 修改
     *
     * @param entityName
     * @return
     */
    private String updateEntity(String entityName) {
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityName);
        String lowerName = MyStringUtil.toLowerCaseFirstOne(entityName);
        StringBuffer sb = new StringBuffer();
        sb.append("\t/**\n");
        sb.append("\t * 修改" + upperName + "\n");
        sb.append("\t */\n");
        sb.append("\tpublic ResultEntity<" + upperName + "> update" + upperName + "(" + upperName + " " + lowerName + ") throws DaoException{\n");
        sb.append("\t\t" + lowerName + "Service.update" + upperName + "(" + lowerName + ");\n");
        sb.append("\t\treturn new ResultEntity<" + upperName + ">(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);\n");
        sb.append("\t}\n\n");
        return sb.toString();
    }


    /**
     * 删除
     *
     * @param entityName
     * @return
     */
    private String deleteEntity(String entityName,String primaryKey) {
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityName);
        String lowerName = MyStringUtil.toLowerCaseFirstOne(entityName);
        StringBuffer sb = new StringBuffer();
        sb.append("\t/**\n");
        sb.append("\t * 删除" + upperName + "\n");
        sb.append("\t */\n");
        sb.append("\tpublic ResultEntity<" + upperName + "> delete" + upperName + "(Map<String, Object> map) throws DaoException{\n");
        
        sb.append("\t\tString "+primaryKey+"s = (String) map.get(\""+primaryKey+"s\");\n");
        sb.append("\t\tif (StringUtil.isNotNull("+primaryKey+"s) && !RegexUtil.isCharAndQuot("+primaryKey+"s)) {\n");
        sb.append("\t\t\tString newIds = StringUtil.strToCharAndQuot("+primaryKey+"s);\n");
        sb.append("\t\t\tmap.put(\""+primaryKey+"s\",newIds);\n");
        sb.append("\t\t}\n");
        
        sb.append("\t\t" + lowerName + "Service.delete" + upperName + "(map);\n");
        sb.append("\t\treturn new ResultEntity<" + upperName + ">(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);\n");
        sb.append("\t}\n\n");
        return sb.toString();
    }
}
